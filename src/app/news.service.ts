import { HttpClient } from '@angular/common/http';
import { Injectable, Input } from '@angular/core';
import { News, Page } from './entities';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private http: HttpClient) { }

  postNews(news:News) {
    return this.http.post<News>('/news', news)
  }

  getAll(page = 0, pageSize =5 ) {
    return this.http.get<Page<News>>('/news',{
      params:{page,pageSize}

    }) ;
  }

  ngOnInit(){

  }

}
