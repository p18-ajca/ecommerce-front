import { TestBed } from '@angular/core/testing';

import { SpecialOrderService } from './special-order.service';

describe('SpecialOrderService', () => {
  let service: SpecialOrderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SpecialOrderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
