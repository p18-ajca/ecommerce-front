import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {UserService} from "./user.service";

@Injectable({
  providedIn: 'root'
})

export class AdminGuard implements CanActivate {

  constructor(private us:UserService, private route:Router) {
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if(this.us.state.user?.role == "ROLE_ADMIN") {
      return true;
    }
    //TODO: renvoyer vers une page solo pour debug, type "/reservee-admin"
    this.route.navigate(['/'])
    return false;
  }

}
