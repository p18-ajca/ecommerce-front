export interface News {
  id?:number;
  title:string;
  content:string;
  imageUrl?:string;
  date:Date;
}

export interface Page<T> {
  content: T[];

  totalPages: number;
  totalElements: number;
  last: boolean;
  first: boolean;

  size: number;
  number: number;
  numberOfElements: number;
  empty: boolean;
}
export interface Category {
  id?:number;
  label:string;
  products:Product[];
}

export interface User {
  id?:number;
  firstName:string;
  lastName:string;
  gender:string;
  date:Date;
  address:string;
  city:string;
  zipCode:string;
  phoneNumber:string;
  role?:string;
  email:string;
  password:string;
}

export interface Product {
  id?:number;
  title:string;
  imageUrl:string;
  author:string;
  pages:number;
  editor:string;
  price:number;
  categories?:Category[];
}

export interface CommandItem {
  id?:number;
  price:number;
  quantity:number;
  product:Product;
}

export interface Command {
  id?:number;
  adress:string;
  status:string;
  date: Date;
  orderItems:CommandItem[];
}

export interface SpecialOrder {
  id?:number;
  firstName:string;
  lastName:string;
  email:string;
  phoneNumber: number;
  title:string;
  author:string;
  language:string;
  status: string;

}
