import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommandItemService } from '../command-item.service';
import { CommandService } from '../command.service';
import { Command, CommandItem, Product } from '../entities';
import { ProductService } from '../product.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit {

  constructor(
    private cs:CommandService,
    private us:UserService,
    private ps:ProductService,
    private cis:CommandItemService,
    private router:Router,
  ){ }

  authState = this.us.state;
  command?:Command;
  orderItems:CommandItem[] = [];

  deleteItem(id:number):void {
    let index:number = 0;
    this.orderItems.forEach(item => {
      if(item.id == id) {
        this.orderItems.splice(index, 1);
      }
      index++
    })
    this.cis.delete(id).subscribe();
  }

  sendCommand() {
    if (this.command != null) {
      this.cs.sendCommand(this.command).subscribe()
    }
  }

  ngOnInit(): void {
    this.cs.getCurrentUserBasket(this.authState.user?.id!).subscribe(data => {
      console.log(data);
      this.command = data,
      console.log(data.orderItems)
      this.orderItems = data.orderItems
    });
  }
}
