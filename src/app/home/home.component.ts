import { Component, OnInit } from '@angular/core';
import { News, Page } from '../entities';
import { NewsService } from '../news.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    news?: Page<News>;
    page = 0;
    pageSize = 5;
  

  constructor(private ns:NewsService) { }

  ngOnInit(): void {
    this.ns.getAll().subscribe(newsList => this.news = newsList)
  }

  nextPage() {
    this.page++;
    this.fetchNews();
  }

  previousPage() {
    this.page--;
    this.fetchNews();
  }



  fetchNews() {
    this.ns.getAll(this.page, this.pageSize).subscribe(data => this.news = data);
  }

}
