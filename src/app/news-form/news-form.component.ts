import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { News } from '../entities';
import { NewsService } from '../news.service';


@Component({
  selector: 'app-news-form',
  templateUrl: './news-form.component.html',
  styleUrls: ['./news-form.component.css']
})
export class NewsFormComponent implements OnInit {

  constructor(private ns:NewsService, private router:Router) { }

  newsForm = new FormGroup ({
   
      title: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      imageURL: new FormControl('', Validators.required),
      date: new FormControl('', Validators.required)
    })
  

  get title() {
    return this.newsForm.get("title")?.value;
  }
  get description() {
    return this.newsForm.get("description")?.value;
  }
  get imageURL() {
    return this.newsForm.get("imageURL")?.value;
  }

  

  onSubmit() {
    let news:News = {
      title: this.title,
      content:this.description,
      imageUrl: this.imageURL,
      date: new Date(Date.now())
    };
    console.log(news);

    this.ns.postNews(news).subscribe();

  }

  ngOnInit(): void {
      
  }
}
