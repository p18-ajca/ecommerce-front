import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validator, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../entities';
import { UserService } from '../user.service';
import {identity} from "rxjs";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  hide1: boolean = true;
  hide2:boolean = true;
  hasError:boolean = false;
  buttonText:string = 'Suivant';
  currentState:string = 'identity';

  registerForm = new FormGroup({
    identity: new FormGroup ({
      firstName: new FormControl('', [Validators.required, Validators.min(2)]),
      lastName: new FormControl('', Validators.required),
      gender: new FormControl('', Validators.required),
      birthDate: new FormControl(Validators.required)
    }),
    contacts: new FormGroup({
      address: new FormControl('', [Validators.required, Validators.min(4)]),
      city: new FormControl('', [Validators.required, Validators.min(4)]),
      zipCode: new FormControl('', [Validators.required, Validators.min(6)]),
      phoneNumber: new FormControl('', Validators.required)
    }),
    logs : new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
      verifyPassword: new FormControl('', Validators.required)
    })
  });
  //Identity :

  get firstName() {
    return this.registerForm.get("identity")?.get("firstName")?.value;
  }
  get lastName() {
    return this.registerForm.get("identity")?.get("lastName")?.value;
  }
  get gender() {
    return this.registerForm.get("identity")?.get("gender")?.value;
  }
  get birthDate() {
    return this.registerForm.get("identity")?.get("birthDate")?.value;
  }
  //Contacts :
  get address() {
    return this.registerForm.get("contacts")?.get("address")?.value;
  }
  get city() {
    return this.registerForm.get("contacts")?.get("city")?.value;
  }
  get zipCode() {
    return this.registerForm.get("contacts")?.get("zipCode")?.value;
  }
  get phoneNumber() {
    return this.registerForm.get("contacts")?.get("phoneNumber")?.value;
  }
  //Logs :
  get email() {
    return this.registerForm.get("logs")?.get("email")?.value;
  }
  get password() {
    return this.registerForm.get("logs")?.get("password")?.value;
  }
  get verifyPassword() {
    return this.registerForm.get("logs")?.get("verifyPassword")?.value;
  }

  constructor(private us:UserService, private router:Router) { }

  onSubmit() {
        let newUser:User = {
          firstName: this.firstName,
          lastName: this.lastName,
          gender: this.gender,
          date: this.birthDate,
          address: this.address,
          city: this.city,
          zipCode: this.zipCode,
          email: this.email,
          phoneNumber: this.phoneNumber,
          password: this.password
        };
        this.us.register(newUser).subscribe({
          next: data => this.router.navigate(['/welcome']),
          error: () => this.hasError = true
        })
    }

  next() {
    switch (this.currentState) {
      case "identity":
        this.currentState = "contacts";
        this.buttonText = "Suivant";
        break;
      case "contacts":
        this.currentState = "logs"
        this.buttonText = "Créer mon compte";
    }
  }

  backButton() {
    switch(this.currentState) {
      case "contacts":
        this.currentState = "identity";
        this.buttonText = "Suivant"
        break;
      case "logs":
        this.currentState = "contacts"
        this.buttonText = "Suivant"
        break;
    }

  }

  ngOnInit(): void {
  }

}
