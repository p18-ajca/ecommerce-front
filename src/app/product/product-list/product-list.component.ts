import { AfterViewInit, Component, ViewChild } from '@angular/core';
// import { MatPaginator } from '@angular/material/paginator';
import { MatTable } from '@angular/material/table';
import { ProductListDataSource, ProductListItem } from './product-list-datasource';
import {ProductService} from "../../product.service";
import {Product} from "../../entities";

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements AfterViewInit {
  // @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatTable) table!: MatTable<Product>;
  // dataSource: ProductListDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = [
    'id',
    'title',
    'author',
    'editor',
    'price',
    'pages',
    'categories',
    'imageUrl',
    // edit/delete actions:
    'actions',
];

  constructor(private productService:ProductService) {
    // this.dataSource = new ProductListDataSource();
  }

  ngAfterViewInit(): void {
    // this.dataSource.paginator = this.paginator;
    // this.table.dataSource = this.dataSource;
    this.fetchRows();

  }

  private fetchRows() {
    this.productService.getAll().subscribe(productList => this.table.dataSource = productList)
    this.table.renderRows();
  }

  deleteRow(id:number) {
    // FIXME
    console.log(id);
    // Est-ce que le deleteProduct passe la main à fetchRows ou est-ce qu'il attend qu'il finisse? ~CA
    this.productService.deleteProduct(id).subscribe(() => this.fetchRows());
  }
}
