import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommandItemService } from '../command-item.service';
import { Product, CommandItem } from '../entities';
import { UserService } from '../user.service';

@Component({
  selector: 'app-add-to-basket-btn',
  templateUrl: './add-to-basket-btn.component.html',
  styleUrls: ['./add-to-basket-btn.component.css']
})
export class AddToBasketBtnComponent implements OnInit {

  constructor(private cis:CommandItemService, private us:UserService, private router:Router) { }

  authUser = this.us.state;

  @Input()
  product?:Product;

  addToBasket() {
    if(this.us.state.user) {
      let commandItem:CommandItem = {quantity: 1, price: this.product?.price!, product: this.product!}
      this.cis.add(commandItem).subscribe();
      this.router.navigate(['/basket'])
    } else {
      this.router.navigate(['/login'])
    }
  }

  ngOnInit(): void {
  }

}
