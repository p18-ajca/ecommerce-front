import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Category, User} from './entities';
import { tap } from 'rxjs';

export interface AuthState {
  user:User|null;
}

@Injectable({
  providedIn: 'root'
})

export class UserService {

  state:AuthState = {
    user: null
  };

  constructor(private http:HttpClient) {
    const stored = localStorage.getItem('user');
    if (stored) {
      this.state.user = JSON.parse(stored);
    }
   }

  /**
   * getAll naïf sans pagination
   */
  getAll() {
    return this.http.get<User[]>('/user');
  }

  getUser() {
    return this.http.get<User>('/user/account')
      .pipe(tap(data => this.state.user = data));
  }

  login(email: string, password: string) {
    return this.http.get<User>('/user/account', {
      headers : {
        'Authorization': 'Basic ' + btoa(email+':'+password),
      }
    }).pipe(tap(data => this.state.user = data))
  }

  register(user:User) {
    return this.http.post<User>('/user', user)
      .pipe(tap(data => this.state.user = data));
  }

  logout() {
    return this.http.get<void>('/logout')
      .pipe(tap(() => this.state.user = null)
      );
  }

  private updateUser(data: User|null) {
    this.state.user = data;
    if(data) {
      localStorage.setItem('user', JSON.stringify(data));
    } else {
      localStorage.removeItem('user');
    }
  }

}
