import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { BasketComponent } from './basket/basket.component';
import { ProfilComponent } from './profil/profil.component';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from "@angular/material/icon";
import { MatToolbarModule} from '@angular/material/toolbar';
import { CardComponent } from './card/card.component';
import {MatCardModule} from '@angular/material/card';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { FormsModule } from '@angular/forms';
import { CredentialsInterceptor } from './credentials.interceptor';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { RegisterComponent } from './register/register.component';
import {MatSelectModule} from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import {MatDividerModule} from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import {MatSidenavModule} from '@angular/material/sidenav';
import {FlexLayoutModule} from '@angular/flex-layout';

import { CategoryPagesComponent } from './category-pages/category-pages.component';
import { ProductCardComponent } from './product-card/product-card.component';
import { NewsFormComponent } from './news-form/news-form.component';
import { ContactsComponent } from './contacts/contacts.component';
import { BasketCardComponent } from './basket-card/basket-card.component';
import { AddProductComponent } from './add-product/add-product.component';
import { OrderSentComponent } from './order-sent/order-sent.component';
import { AddToBasketBtnComponent } from './add-to-basket-btn/add-to-basket-btn.component';
import { NonAuthComponent } from './non-auth/non-auth.component';
import { SpecialOrderFormComponent } from './special-order-form/special-order-form.component';
import { SingleProductComponent } from './single-product/single-product.component';
import { SingleProductBtnComponent } from './single-product-btn/single-product-btn.component';
import { UserListComponent } from './user/user-list/user-list.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ProductListComponent } from './product/product-list/product-list.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { SingleCategoryComponent } from './single-category/single-category.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BasketComponent,
    ProfilComponent,
    CardComponent,
    LoginComponent,
    WelcomeComponent,
    RegisterComponent,
    CategoryPagesComponent,
    ProductCardComponent,
    NewsFormComponent,
    ContactsComponent,
    BasketCardComponent,
    AddProductComponent,
    OrderSentComponent,
    AddToBasketBtnComponent,
    NonAuthComponent,
    UserListComponent,
    SpecialOrderFormComponent,
    SingleProductComponent,
    SingleProductBtnComponent,
    ProductListComponent,
    EditProductComponent,
    SingleCategoryComponent,



  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatCardModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDividerModule,
    MatListModule,
    MatSidenavModule,
    FlexLayoutModule,
    MatTableModule,
    MatPaginatorModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: CredentialsInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
