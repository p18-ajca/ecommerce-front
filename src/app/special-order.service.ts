import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SpecialOrder} from './entities';

@Injectable({
  providedIn: 'root'
})
export class SpecialOrderService {

  constructor(private http: HttpClient) { }
  
  PostSpecialOrder(specialOrder:SpecialOrder){
    return this.http.post<SpecialOrder>("/specialorder", specialOrder)
  }

  ngOnInit(){}

}

