import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTable } from '@angular/material/table';
// import { UserListDataSource, UserListItem } from './user-list-datasource';
import {UserService} from "../../user.service";
import {Observable} from "rxjs";
import {User} from "../../entities";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements AfterViewInit {
  // @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatTable) table!: MatTable<User>;
  // dataSource: Observable<User[]>;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered.
   *
   * l'ordre des colonnes est défini ici et pas dans le component.html !!! */
  displayedColumns = [
    'id',
    'name',
    'email',
    'gender',
    'signupDate',
    'fullAddress',
    'phoneNumber',
    'role',
  ];

  constructor(private userService:UserService) {
    // this.dataSource = new UserListDataSource(userService);
    // this.dataSource = userService.getAll();
  }

  ngAfterViewInit(): void {
    // this.dataSource.paginator = this.paginator;
    // this.table.dataSource = this.dataSource;
    // modele à partir des news (HomeComponent):
    // this.ns.getAll().subscribe(newsList => this.news = newsList)
    this.fetchRows();
    this.table.renderRows();
  }

  private fetchRows(): void {
    this.userService.getAll().subscribe(userList => this.table.dataSource = userList)
  }
}
