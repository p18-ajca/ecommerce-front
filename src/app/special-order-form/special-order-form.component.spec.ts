import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialOrderFormComponent } from './special-order-form.component';

describe('SpecialOrderFormComponent', () => {
  let component: SpecialOrderFormComponent;
  let fixture: ComponentFixture<SpecialOrderFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpecialOrderFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialOrderFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
