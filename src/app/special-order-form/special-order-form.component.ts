import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SpecialOrder } from '../entities';
import { SpecialOrderService } from '../special-order.service';

@Component({
  selector: 'app-special-order-form',
  templateUrl: './special-order-form.component.html',
  styleUrls: ['./special-order-form.component.css']
})
export class SpecialOrderFormComponent implements OnInit {

  constructor(private sp:SpecialOrderService, private router:Router) { }

  SpecialOrderForm = new FormGroup({
    title: new FormControl ('', Validators.required),
    author: new FormControl ('', Validators.required),
    language: new FormControl ('', Validators.required),
    lastname: new FormControl ('', Validators.required),
    firstname: new FormControl ('', Validators.required),
    email: new FormControl ('', Validators.required),
    phonenumber: new FormControl ('', Validators.required)
  })

  get title() {
    return this.SpecialOrderForm.get("title")?.value;
  }
  get author() {
    return this.SpecialOrderForm.get("author")?.value;
  }
  get language() {
    return this.SpecialOrderForm.get("language")?.value;
  }
  get lastname() {
    return this.SpecialOrderForm.get("lastname")?.value;
  }
  get firstname() {
    return this.SpecialOrderForm.get("firstname")?.value;
  }
  get email() {
    return this.SpecialOrderForm.get("email")?.value;
  }
  get phonenumber() {
    return this.SpecialOrderForm.get("phonenumber")?.value;
  }

  onSubmit() {
    let specialOrder:SpecialOrder ={
      title: this.title,
      author: this.author,
      language: this.language,
      lastName: this.lastname,
      firstName: this.firstname,
      phoneNumber: this.phonenumber,
      email: this.email,
      status: "Asked"
    };
    console.log(specialOrder);

    this.sp.PostSpecialOrder(specialOrder).subscribe();
  }

  ngOnInit(): void {
  }

}
