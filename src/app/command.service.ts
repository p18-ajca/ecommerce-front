import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Command } from './entities';

@Injectable({
  providedIn: 'root'
})
export class CommandService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<Command[]>('/command');
  }

  getCurrentUserBasket(currentUserId:number) {
    return this.http.get<Command>('/command/unique/' + currentUserId);
  }

  sendCommand(command:Command) {
    return this.http.put<Command>('/command/done/' + command.id, command);
  }
}
