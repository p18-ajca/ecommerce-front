import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoryPagesComponent } from './category-pages/category-pages.component';
import { BasketComponent } from './basket/basket.component';
import { ContactsComponent } from './contacts/contacts.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { NewsFormComponent } from './news-form/news-form.component';
import { ProductCardComponent } from './product-card/product-card.component';
import { RegisterComponent } from './register/register.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { AddProductComponent } from './add-product/add-product.component';
import { OrderSentComponent } from './order-sent/order-sent.component';
import { SpecialOrderFormComponent } from './special-order-form/special-order-form.component';
import {AuthGuard} from "./auth.guard";
import {SingleProductComponent} from "./single-product/single-product.component";
import {UserListComponent} from "./user/user-list/user-list.component";
import {ProductListComponent} from "./product/product-list/product-list.component";
import {EditProductComponent} from "./edit-product/edit-product.component";
import { AdminGuard } from './admin.guard';
import {SingleCategoryComponent} from "./single-category/single-category.component";

const routes: Routes = [
  {path: '', component: HomeComponent},                                           // (news#index)
  {path: 'login', component: LoginComponent},                                     // user/login
  {path: 'welcome', component:WelcomeComponent, canActivate:[AuthGuard]},         // user/welcome
  {path: 'register', component:RegisterComponent},                                // user/register
  {path: 'product-card', component:ProductCardComponent},                         // - sub-component
  {path: 'category', component:CategoryPagesComponent}, // ou alors :id optionnel?   category
  {path: 'category/:id', component:SingleCategoryComponent},                      //  category/:id
  {path: 'contacts', component:ContactsComponent},                                // contact/
  {path: 'special-order-form', component:SpecialOrderFormComponent},              // special-order/??
  {path: 'basket', component:BasketComponent, canActivate:[AuthGuard]},           // ?? /order/basket/
  {path: 'add-product', component:AddProductComponent, canActivate:[AdminGuard]}, // product/add
  {path: 'order-done', component:OrderSentComponent, canActivate:[AuthGuard]},    // order/done ?
  {path: 'single-product/:id', component:SingleProductComponent},                 // product/:id à la place du edit
  {path: 'user', component:UserListComponent, canActivate:[AdminGuard]},          // user
  {path: 'news-form', component:NewsFormComponent, canActivate:[AdminGuard]},     // news/edit/:id ?
  {path: 'product', component:ProductListComponent, canActivate:[AdminGuard]},    // /product
  {path: 'product/:id', component:EditProductComponent, canActivate:[AdminGuard]}, // product/edit/:id ?
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
