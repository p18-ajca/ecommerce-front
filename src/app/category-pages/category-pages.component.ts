import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../category.service';
import { Category, Product } from '../entities';

@Component({
  selector: 'app-category-pages',
  templateUrl: './category-pages.component.html',
  styleUrls: ['./category-pages.component.css']
})
export class CategoryPagesComponent implements OnInit {

  categories: Category[] = [];
  idCategory:number = 0;
  products: Product[] = [];


  // justify = this.idCategory = 1 ? this.idCategory : this.idCategory -1  ;
  constructor(private categoryService: CategoryService) { }

  ngOnInit(): void {

    this.categoryService.getAll().subscribe(data => this.categories = data);
  }


  fetchByCategory() {
    this.categoryService.getProducts(this.idCategory).subscribe(data => this.products = data);
  }
}
