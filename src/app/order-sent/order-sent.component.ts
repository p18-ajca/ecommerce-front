import { Component, OnInit } from '@angular/core';
import { UserService, AuthState } from '../user.service';

@Component({
  selector: 'app-order-sent',
  templateUrl: './order-sent.component.html',
  styleUrls: ['./order-sent.component.css']
})
export class OrderSentComponent implements OnInit {

  constructor(private us:UserService) { }

  currentUser = this.us.state;

  ngOnInit(): void {
  }

}
