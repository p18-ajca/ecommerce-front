
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import { CategoryService } from '../category.service';
import { Category, Product } from '../entities';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  allCategories: Category[] = [];

  currentProduct?:Product;

  routeId?:number;

  constructor(private productService: ProductService, private categoryService: CategoryService, private router: Router, private route:ActivatedRoute) { }

  productForm = new FormGroup({
    title: new FormControl('', Validators.required),
    imageUrl: new FormControl('', Validators.required),
    author: new FormControl('', Validators.required),
    pages: new FormControl('', Validators.required),
    editor: new FormControl('', Validators.required),
    price: new FormControl('', Validators.required),
    categories: new FormControl('', Validators.required)
  });



  get title() {
    return this.productForm.get("title")?.value;
  }

  get imageUrl() {
    return this.productForm.get("imageUrl")?.value;
  }

  get author() {
    return this.productForm.get("author")?.value;
  }

  get pages() {
    return this.productForm.get("pages")?.value;
  }

  get editor() {
    return this.productForm.get("editor")?.value;
  }

  get  price() {
    return this.productForm.get("price")?.value;
  }

  get categories() {
    return this.productForm.get("categories")?.value;
  }


  onSubmit() {

    let newProduct:Product = {
      title: this.title,
      imageUrl: this.imageUrl,
      author: this.author,
      pages: this.pages,
      editor: this.editor,
      price: this.price,
      categories: this.categories,
    };

    console.log(this.categories)

    // la catégorie est le membre "fort" de la relation ManyToMany donc on ajoute le produit à la catégorie
    // (mais une seule categorie par produit pour l'instant malgré le many to many)
    // TODO j'aurais plus besoin de ça
    this.categoryService.addProductToCategory(newProduct, newProduct.categories!).subscribe();


    // if(this.productForm.valid)
    // this.addProducts.addProduct(this.productForm.value).subscribe(() => {
    //   this.router.navigate(['/']);
    // });
    // else(this.productForm.valid)

  }
  ngOnInit(): void {
    this.route.params.subscribe(param => {
      this.routeId = Number(param['id']);
    });
    this.productService.getById(this.routeId!).subscribe(data => { this.currentProduct = data;});

    this.categoryService.getAll().subscribe(data => this.allCategories = data);


  }

 /* TODO: https://stackoverflow.com/a/50049506
  Use compareWith, A function to compare the option values with the selected values.
  see here: https://material.angular.io/components/select/api#MatSelect

  For an object of the following structure:

  listOfObjs = [{ name: 'john', id: '1'}, { name: 'jimmy', id: '2'},...]

  Define markup like this:

  <mat-form-field>
    <mat-select
      [compareWith]="compareObjects"
        [(ngModel)]="obj">
      <mat-option  *ngFor="let obj of listOfObjs" [value]="obj">
        {{ obj.name }}
    </mat-option>
    </mat-select>
    </mat-form-field>

  And define comparison function like this:

  compareObjects(o1: any, o2: any): boolean {
    return o1.name === o2.name && o1.id === o2.id;
  }
*/
}

