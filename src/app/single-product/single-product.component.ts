import { Component, OnInit } from '@angular/core';
import {Product} from "../entities";
import {ProductService} from "../product.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-single-product',
  templateUrl: './single-product.component.html',
  styleUrls: ['./single-product.component.css']
})
export class SingleProductComponent implements OnInit {

  routeId?:string;

  product?:Product;

  constructor(private ps:ProductService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(param => {
      this.routeId = param['id'];
    });
    this.ps.getById(Number(this.routeId)).subscribe(data => this.product = data);
  }
}
