
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CategoryService } from '../category.service';
import { Category, Product } from '../entities';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  categories: Category[] = [];

  constructor(private addProducts: ProductService, private categoryService: CategoryService, private router: Router) { }

  productForm = new FormGroup({
    title: new FormControl('', Validators.required),
    imageUrl: new FormControl('', Validators.required),
    author: new FormControl('', Validators.required),
    pages: new FormControl('', Validators.required),
    editor: new FormControl('', Validators.required),
    price: new FormControl('', Validators.required),
    category: new FormControl('', Validators.required)
  });

  get title() {
    return this.productForm.get("title")?.value;
  }

  get imageUrl() {
    return this.productForm.get("imageUrl")?.value;
  }

  get author() {
    return this.productForm.get("author")?.value;
  }

  get pages() {
    return this.productForm.get("pages")?.value;
  }

  get editor() {
    return this.productForm.get("editor")?.value;
  }

  get price() {
    return this.productForm.get("price")?.value;
  }
  get category() {
    return this.productForm.get("category")?.value;
  }

  onSubmit() {

    let newProduct:Product = {
      title: this.title,
      imageUrl: this.imageUrl,
      author: this.author,
      pages: this.pages,
      editor: this.editor,
      price: this.price,
    };

    console.log(this.category)
    this.addProducts.addProduct(newProduct, this.category).subscribe();


    // if(this.productForm.valid)
    // this.addProducts.addProduct(this.productForm.value).subscribe(() => {
    //   this.router.navigate(['/']);
    // });
    // else(this.productForm.valid)

  }
  ngOnInit(): void {
    this.categoryService.getAll().subscribe(data => this.categories = data);
  }
}

