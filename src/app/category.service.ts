import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Category, Product } from './entities';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<Category[]>('/category');
  }

  // unused
  getOne(id:number) {
    return this.http.get<Category>("/category/"+id);
  }

  // breaks separation of concerns
  getProducts(categoryId:number) {
    return this.http.get<Product[]>("/category/"+categoryId+"/product");
  }

  addProductToCategory(product:Product, categories:Category[]) {
    // TODO: this isn't actually implemented on the server...
    return this.http.post<Product[]>("/category/" + categories[0].id + "/product" + product.id, product);
  }
}

