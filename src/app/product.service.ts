import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Category, Product, User} from './entities';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<Product[]>('/product/');
  }

  getById(id: number) {
    return this.http.get<Product>('/product/' + id);
  }

  addProduct(product: Product, id:number) {
    console.log(product);
    return this.http.post<Product>('/product/'+id, product);

  }

  updateProduct(product: Product, id:number) {
    return this.http.put<Product>('/product/' + id, product)
  }

  deleteProduct(id:number) {
    return this.http.delete<Product>('/product/' + id);
  }


}
