import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommandItem } from './entities';

@Injectable({
  providedIn: 'root'
})
export class CommandItemService {

  constructor(private http: HttpClient) { }

  delete(id:number) {
    return this.http.delete('/commanditem/' + id);
  }

  add(commandItem:CommandItem) {
    return this.http.post('/commanditem', commandItem);
  }

}
