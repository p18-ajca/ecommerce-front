import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleProductBtnComponent } from './single-product-btn.component';

describe('SingleProductBtnComponent', () => {
  let component: SingleProductBtnComponent;
  let fixture: ComponentFixture<SingleProductBtnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SingleProductBtnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleProductBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
