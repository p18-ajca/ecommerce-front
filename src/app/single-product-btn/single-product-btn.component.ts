import {Component, Input, OnInit} from '@angular/core';
import {Product} from "../entities";
import {Router} from "@angular/router";

@Component({
  selector: 'app-single-product-btn',
  templateUrl: './single-product-btn.component.html',
  styleUrls: ['./single-product-btn.component.css']
})
export class SingleProductBtnComponent implements OnInit {

  @Input()
  product?:Product

  constructor(private route:Router) { }

  onRoute() {
    this.route.navigate(['single-product/' + this.product?.id])
  }

  ngOnInit(): void {
  }

}
