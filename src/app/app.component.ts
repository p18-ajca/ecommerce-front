import { Component } from '@angular/core';
import { UserService } from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ecommerce-front';
  authState = this.us.state;
  constructor(private us:UserService) {}

  logout() {
    this.us.logout().subscribe();
  }

}
