import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommandItem } from '../entities';

@Component({
  selector: 'app-basket-card',
  templateUrl: './basket-card.component.html',
  styleUrls: ['./basket-card.component.css']
})
export class BasketCardComponent implements OnInit {

  constructor() { }

  @Input()
  orderItem?:CommandItem;
  @Output() 
  deleteItem: EventEmitter<void> = new EventEmitter();

  clickDelete() {
    this.deleteItem.emit()
  }

  ngOnInit(): void {
  }

}
