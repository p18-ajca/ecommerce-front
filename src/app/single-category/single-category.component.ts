import { Component, OnInit } from '@angular/core';
import { CategoryService } from "../category.service";
import {Category, Product} from "../entities";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-single-category',
  templateUrl: './single-category.component.html',
  styleUrls: ['./single-category.component.css']
})
export class SingleCategoryComponent implements OnInit {

  category?: Category;
  categoryId?:number;
  products: Product[] = [];

  constructor(private categoryService:CategoryService, private route:ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(param => {
      this.categoryId = param['id'];
    });
    this.categoryService.getOne(this.categoryId!).subscribe(data => this.category = data)
    this.categoryService.getProducts(this.categoryId!).subscribe(data => this.products = data);
  }
}
