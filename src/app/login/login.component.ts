import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { Validators } from '@angular/forms';
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  hide = true;

  constructor(private us:UserService, private router:Router) { }

  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email] ),
    password: new FormControl('', Validators.required)
  });

  get email() {
    return this.loginForm.get('email');
  }

  get password() {
    return this.loginForm.get('password');
  }

  hasError:string = "";

  login() {
    this.us.login(this.loginForm.value.email, this.loginForm.value.password).subscribe({
      next: data => this.router.navigate(['/welcome']),
      error: (err: any ) => {
        if(err instanceof HttpErrorResponse) {
          if(err.status == 401) {
            this.hasError = "you";
          } else {
            this.hasError = "me";
          }
        }

      }
    });
  }

  ngOnInit(): void {
  }

}
