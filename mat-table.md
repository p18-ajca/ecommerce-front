# Material Table \[mat-table\]

https://material.angular.io/components/table/examples


## DataSource
A `DataSource` is simply a class that has at a minimum the following methods: `connect` and `disconnect`. The `connect` method will be called by the table to provide an `Observable` that emits the data array that should be rendered. The table will call `disconnect` when the table is destroyed, which may be the right time to *clean up any subscriptions* that may have been registered in the `connect` method.

you may want to create your own custom `DataSource` class for more complex use cases. This can be done by extending the abstract `DataSource` class with a custom `DataSource` class that then implements the `connect` and `disconnect` methods.

## Features
The MatTable is focused on a single responsibility: efficiently render rows of data in a performant and accessible way.

You'll notice that the table itself doesn't come out of the box with a lot of features, but expects that the table will be included in a composition of components that fills out its features.

For example, you can add sorting and pagination to the table by using `MatSort` and `MatPaginator` and mutating the data provided to the table according to their outputs.

##  MatTable API

###  MatTable extends [CdkTable](https://material.angular.io/cdk/table/api#CdkTable)

``` typescript
@Input()
dataSource: CdkTableDataSourceInput<T>
```
The table's source of data, which can be provided in three ways (in order of complexity):

- Simple data array (each object represents one table row)
- **Stream that emits a data array each time the array changes** (`Observable` stream)
- `DataSource` object that implements the `connect`/`disconnect` interface.
If a data array is provided, the table must be notified when the array's objects are added, removed, or moved. This can be done by calling the `renderRows()` function which will render the diff since the last table render. If the data array reference is changed, the table will automatically trigger an update to the rows.

**When providing an `Observable` stream, the table will trigger an update automatically when the stream emits a new array of data.**

Finally, when providing a `DataSource` object, the table will use the Observable stream provided by the connect function and trigger updates when that stream emits new data array values. During the table's `ngOnDestroy` or when the data source is removed from the table, the table will call the DataSource's `disconnect` function (may be useful for cleaning up any subscriptions registered during the connect process).



